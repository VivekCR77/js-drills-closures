const { counterFactory } = require("../counterFactory.js");

const { increment, decrement } = counterFactory();

console.log(increment());
console.log(decrement());
