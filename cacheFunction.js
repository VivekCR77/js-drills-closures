/*
Tasks:

1.Should return a function that invokes `cb`.
2.A cache (object) should be kept in closure scope.
3.The cache should keep track of all arguments have been used to invoke this function.
4.If the returned function is invoked with arguments that it has already seen
5.then it should return the cached result and not invoke `cb` again.
6.`cb` should only ever be invoked once for a given set of arguments.

*/

function cb(ele, obj) {
  console.log(`The argument "${ele}" already exist`);
  console.log("Cache object", obj);
}

function cacheFunction(cb) {
  cache = {};
  function memoize(a) {
    if (cache[a]) {
      return cb(a, cache);
    } else {
      cache[a] = 1;
    }
  }

  return memoize;
}

module.exports = { cacheFunction, cb };

/*
const mem = cacheFunction(cb);

// calling function.

arr = [1, 2, 3];
len = arr.length;
for (let index = 0; index <= len; index++) {
  if (mem(arr[index % len])) {
    break;
  } else {
    continue;
  }
}
*/
