const { cacheFunction, cb } = require("../cacheFunction.js");

const mem = cacheFunction(cb);

// calling function.

arr = [1, 2, 3];
len = arr.length;
for (let index = 0; index <= len; index++) {
  if (mem(arr[index % len])) {
    break;
  } else {
    continue;
  }
}
