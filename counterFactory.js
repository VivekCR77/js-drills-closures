/*
Tasks:

1.Return an object that has two methods called `increment` and `decrement`.
2.`increment` should increment a counter variable in closure scope and return it.
3.`decrement` should decrement the counter variable and return it.
*/

function counterFactory() {
  var counter1 = 2;
  var counter2 = 2;

  function increment() {
    return (counter1 += 1);
  }

  function decrement() {
    return (counter2 -= 1);
  }

  return { increment, decrement };
}

module.exports = { counterFactory };

// const { increment, decrement } = counterFactory();

// console.log(increment());
// console.log(decrement());
