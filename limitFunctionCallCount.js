/*
Tasks:

1.Should return a function that invokes `cb`.
2.The returned function should only allow `cb` to be invoked `n` times.
3.Returning null is acceptable if cb can't be returned
*/

function cb(n) {
  return `Invoked ${n} times`;
}

function limitFunctionCallCount(cb, n) {
  function helper(n) {
    for (let i = 1; i <= n; i++) {
      console.log(cb(i));
    }
  }

  return helper;
}

module.exports = { limitFunctionCallCount, cb };

// const limit = limitFunctionCallCount(cb, 5);

// limit(5);
