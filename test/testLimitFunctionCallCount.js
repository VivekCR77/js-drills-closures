const { limitFunctionCallCount, cb } = require("../limitFunctionCallCount.js");

const limit = limitFunctionCallCount(cb, 5);

limit(5);
